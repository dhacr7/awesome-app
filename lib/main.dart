import 'package:awesome_app/presentation/app_widget.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:injectable/injectable.dart';

import 'injection.dart';

void main() {
  configureInjection(Environment.prod);
  debugInstrumentationEnabled = true;
  WidgetsFlutterBinding.ensureInitialized();
  runApp(AppWidget());
}
