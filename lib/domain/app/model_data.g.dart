// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model_data.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ModelResultData _$_$ModelResultDataFromJson(Map<String, dynamic> json) {
  return _$ModelResultData(
    id: json['id'] as int,
    width: json['width'] as int,
    height: json['height'] as int,
    url: json['url'] as String,
    photographer: json['photographer'] as String,
    photographerUrl: json['photographer_url'] as String,
    photographerId: json['photographer_id'] as int,
    avgColor: json['avg_color'] as String,
    liked: json['liked'] as bool,
    src: json['src'] == null
        ? null
        : SrcData.fromJson(json['src'] as Map<String, dynamic>),
    totalResults: json['total_results'] as int,
  );
}

Map<String, dynamic> _$_$ModelResultDataToJson(_$ModelResultData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'width': instance.width,
      'height': instance.height,
      'url': instance.url,
      'photographer': instance.photographer,
      'photographer_url': instance.photographerUrl,
      'photographer_id': instance.photographerId,
      'avg_color': instance.avgColor,
      'liked': instance.liked,
      'src': instance.src,
      'total_results': instance.totalResults,
    };

_$SrcData _$_$SrcDataFromJson(Map<String, dynamic> json) {
  return _$SrcData(
    original: json['original'] as String,
    large2x: json['large2x'] as String,
    large: json['large'] as String,
    medium: json['medium'] as String,
    small: json['small'] as String,
    portrait: json['portrait'] as String,
    landscape: json['landscape'] as String,
    tiny: json['tiny'] as String,
  );
}

Map<String, dynamic> _$_$SrcDataToJson(_$SrcData instance) => <String, dynamic>{
      'original': instance.original,
      'large2x': instance.large2x,
      'large': instance.large,
      'medium': instance.medium,
      'small': instance.small,
      'portrait': instance.portrait,
      'landscape': instance.landscape,
      'tiny': instance.tiny,
    };
