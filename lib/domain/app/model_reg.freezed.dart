// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'model_reg.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
ModelReq _$ModelReqFromJson(Map<String, dynamic> json) {
  return ModelDataPhotos.fromJson(json);
}

class _$ModelReqTearOff {
  const _$ModelReqTearOff();

// ignore: unused_element
  ModelDataPhotos photosResponse(List<ModelResultData> photos) {
    return ModelDataPhotos(
      photos,
    );
  }
}

// ignore: unused_element
const $ModelReq = _$ModelReqTearOff();

mixin _$ModelReq {
  List<ModelResultData> get photos;

  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result photosResponse(List<ModelResultData> photos),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result photosResponse(List<ModelResultData> photos),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result photosResponse(ModelDataPhotos value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result photosResponse(ModelDataPhotos value),
    @required Result orElse(),
  });
  Map<String, dynamic> toJson();
  $ModelReqCopyWith<ModelReq> get copyWith;
}

abstract class $ModelReqCopyWith<$Res> {
  factory $ModelReqCopyWith(ModelReq value, $Res Function(ModelReq) then) =
      _$ModelReqCopyWithImpl<$Res>;
  $Res call({List<ModelResultData> photos});
}

class _$ModelReqCopyWithImpl<$Res> implements $ModelReqCopyWith<$Res> {
  _$ModelReqCopyWithImpl(this._value, this._then);

  final ModelReq _value;
  // ignore: unused_field
  final $Res Function(ModelReq) _then;

  @override
  $Res call({
    Object photos = freezed,
  }) {
    return _then(_value.copyWith(
      photos:
          photos == freezed ? _value.photos : photos as List<ModelResultData>,
    ));
  }
}

abstract class $ModelDataPhotosCopyWith<$Res>
    implements $ModelReqCopyWith<$Res> {
  factory $ModelDataPhotosCopyWith(
          ModelDataPhotos value, $Res Function(ModelDataPhotos) then) =
      _$ModelDataPhotosCopyWithImpl<$Res>;
  @override
  $Res call({List<ModelResultData> photos});
}

class _$ModelDataPhotosCopyWithImpl<$Res> extends _$ModelReqCopyWithImpl<$Res>
    implements $ModelDataPhotosCopyWith<$Res> {
  _$ModelDataPhotosCopyWithImpl(
      ModelDataPhotos _value, $Res Function(ModelDataPhotos) _then)
      : super(_value, (v) => _then(v as ModelDataPhotos));

  @override
  ModelDataPhotos get _value => super._value as ModelDataPhotos;

  @override
  $Res call({
    Object photos = freezed,
  }) {
    return _then(ModelDataPhotos(
      photos == freezed ? _value.photos : photos as List<ModelResultData>,
    ));
  }
}

@JsonSerializable()
class _$ModelDataPhotos implements ModelDataPhotos {
  _$ModelDataPhotos(this.photos) : assert(photos != null);

  factory _$ModelDataPhotos.fromJson(Map<String, dynamic> json) =>
      _$_$ModelDataPhotosFromJson(json);

  @override
  final List<ModelResultData> photos;

  @override
  String toString() {
    return 'ModelReq.photosResponse(photos: $photos)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ModelDataPhotos &&
            (identical(other.photos, photos) ||
                const DeepCollectionEquality().equals(other.photos, photos)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(photos);

  @override
  $ModelDataPhotosCopyWith<ModelDataPhotos> get copyWith =>
      _$ModelDataPhotosCopyWithImpl<ModelDataPhotos>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result photosResponse(List<ModelResultData> photos),
  }) {
    assert(photosResponse != null);
    return photosResponse(photos);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result photosResponse(List<ModelResultData> photos),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (photosResponse != null) {
      return photosResponse(photos);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result photosResponse(ModelDataPhotos value),
  }) {
    assert(photosResponse != null);
    return photosResponse(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result photosResponse(ModelDataPhotos value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (photosResponse != null) {
      return photosResponse(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$_$ModelDataPhotosToJson(this);
  }
}

abstract class ModelDataPhotos implements ModelReq {
  factory ModelDataPhotos(List<ModelResultData> photos) = _$ModelDataPhotos;

  factory ModelDataPhotos.fromJson(Map<String, dynamic> json) =
      _$ModelDataPhotos.fromJson;

  @override
  List<ModelResultData> get photos;
  @override
  $ModelDataPhotosCopyWith<ModelDataPhotos> get copyWith;
}
