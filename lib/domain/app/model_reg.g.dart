// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'model_reg.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$ModelDataPhotos _$_$ModelDataPhotosFromJson(Map<String, dynamic> json) {
  return _$ModelDataPhotos(
    (json['photos'] as List)
        ?.map((e) => e == null
            ? null
            : ModelResultData.fromJson(e as Map<String, dynamic>))
        ?.toList(),
  );
}

Map<String, dynamic> _$_$ModelDataPhotosToJson(_$ModelDataPhotos instance) =>
    <String, dynamic>{
      'photos': instance.photos,
    };
