import 'package:freezed_annotation/freezed_annotation.dart';
import 'model_data.dart';
part 'model_reg.freezed.dart';
part 'model_reg.g.dart';

@freezed
abstract class ModelReq with _$ModelReq {
  ///====================================================
  /// Photos
  ///====================================================
  factory ModelReq.photosResponse(List<ModelResultData> photos) =
      ModelDataPhotos;

  factory ModelReq.fromJson(Map<String, dynamic> json) =>
      _$ModelReqFromJson(json);
}
