import 'package:freezed_annotation/freezed_annotation.dart';

part 'model_data.freezed.dart';
part 'model_data.g.dart';

@freezed
abstract class ModelData with _$ModelData {
  factory ModelData.fromJson(Map<String, dynamic> json) =>
      _$ModelDataFromJson(json);

  factory ModelData.photosResultData({
    @JsonKey(name: "id") int id,
    @JsonKey(name: "width") int width,
    @JsonKey(name: "height") int height,
    @JsonKey(name: "url") String url,
    @JsonKey(name: "photographer") String photographer,
    @JsonKey(name: "photographer_url") String photographerUrl,
    @JsonKey(name: "photographer_id") int photographerId,
    @JsonKey(name: "avg_color") String avgColor,
    @JsonKey(name: "liked") bool liked,
    @JsonKey(name: "src") SrcData src,
    @JsonKey(name: "total_results") int totalResults,

    /// ==========================================
  }) = ModelResultData;

  factory ModelData.srcData({
    @JsonKey(name: "original") String original,
    @JsonKey(name: "large2x") String large2x,
    @JsonKey(name: "large") String large,
    @JsonKey(name: "medium") String medium,
    @JsonKey(name: "small") String small,
    @JsonKey(name: "portrait") String portrait,
    @JsonKey(name: "landscape") String landscape,
    @JsonKey(name: "tiny") String tiny,
  }) = SrcData;
}
