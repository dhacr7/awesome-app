// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'model_data.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;
ModelData _$ModelDataFromJson(Map<String, dynamic> json) {
  switch (json['runtimeType'] as String) {
    case 'photosResultData':
      return ModelResultData.fromJson(json);
    case 'srcData':
      return SrcData.fromJson(json);

    default:
      throw FallThroughError();
  }
}

class _$ModelDataTearOff {
  const _$ModelDataTearOff();

// ignore: unused_element
  ModelResultData photosResultData(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'width') int width,
      @JsonKey(name: 'height') int height,
      @JsonKey(name: 'url') String url,
      @JsonKey(name: 'photographer') String photographer,
      @JsonKey(name: 'photographer_url') String photographerUrl,
      @JsonKey(name: 'photographer_id') int photographerId,
      @JsonKey(name: 'avg_color') String avgColor,
      @JsonKey(name: 'liked') bool liked,
      @JsonKey(name: 'src') SrcData src,
      @JsonKey(name: 'total_results') int totalResults}) {
    return ModelResultData(
      id: id,
      width: width,
      height: height,
      url: url,
      photographer: photographer,
      photographerUrl: photographerUrl,
      photographerId: photographerId,
      avgColor: avgColor,
      liked: liked,
      src: src,
      totalResults: totalResults,
    );
  }

// ignore: unused_element
  SrcData srcData(
      {@JsonKey(name: 'original') String original,
      @JsonKey(name: 'large2x') String large2x,
      @JsonKey(name: 'large') String large,
      @JsonKey(name: 'medium') String medium,
      @JsonKey(name: 'small') String small,
      @JsonKey(name: 'portrait') String portrait,
      @JsonKey(name: 'landscape') String landscape,
      @JsonKey(name: 'tiny') String tiny}) {
    return SrcData(
      original: original,
      large2x: large2x,
      large: large,
      medium: medium,
      small: small,
      portrait: portrait,
      landscape: landscape,
      tiny: tiny,
    );
  }
}

// ignore: unused_element
const $ModelData = _$ModelDataTearOff();

mixin _$ModelData {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required
        Result photosResultData(
            @JsonKey(name: 'id') int id,
            @JsonKey(name: 'width') int width,
            @JsonKey(name: 'height') int height,
            @JsonKey(name: 'url') String url,
            @JsonKey(name: 'photographer') String photographer,
            @JsonKey(name: 'photographer_url') String photographerUrl,
            @JsonKey(name: 'photographer_id') int photographerId,
            @JsonKey(name: 'avg_color') String avgColor,
            @JsonKey(name: 'liked') bool liked,
            @JsonKey(name: 'src') SrcData src,
            @JsonKey(name: 'total_results') int totalResults),
    @required
        Result srcData(
            @JsonKey(name: 'original') String original,
            @JsonKey(name: 'large2x') String large2x,
            @JsonKey(name: 'large') String large,
            @JsonKey(name: 'medium') String medium,
            @JsonKey(name: 'small') String small,
            @JsonKey(name: 'portrait') String portrait,
            @JsonKey(name: 'landscape') String landscape,
            @JsonKey(name: 'tiny') String tiny),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result photosResultData(
        @JsonKey(name: 'id') int id,
        @JsonKey(name: 'width') int width,
        @JsonKey(name: 'height') int height,
        @JsonKey(name: 'url') String url,
        @JsonKey(name: 'photographer') String photographer,
        @JsonKey(name: 'photographer_url') String photographerUrl,
        @JsonKey(name: 'photographer_id') int photographerId,
        @JsonKey(name: 'avg_color') String avgColor,
        @JsonKey(name: 'liked') bool liked,
        @JsonKey(name: 'src') SrcData src,
        @JsonKey(name: 'total_results') int totalResults),
    Result srcData(
        @JsonKey(name: 'original') String original,
        @JsonKey(name: 'large2x') String large2x,
        @JsonKey(name: 'large') String large,
        @JsonKey(name: 'medium') String medium,
        @JsonKey(name: 'small') String small,
        @JsonKey(name: 'portrait') String portrait,
        @JsonKey(name: 'landscape') String landscape,
        @JsonKey(name: 'tiny') String tiny),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result photosResultData(ModelResultData value),
    @required Result srcData(SrcData value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result photosResultData(ModelResultData value),
    Result srcData(SrcData value),
    @required Result orElse(),
  });
  Map<String, dynamic> toJson();
}

abstract class $ModelDataCopyWith<$Res> {
  factory $ModelDataCopyWith(ModelData value, $Res Function(ModelData) then) =
      _$ModelDataCopyWithImpl<$Res>;
}

class _$ModelDataCopyWithImpl<$Res> implements $ModelDataCopyWith<$Res> {
  _$ModelDataCopyWithImpl(this._value, this._then);

  final ModelData _value;
  // ignore: unused_field
  final $Res Function(ModelData) _then;
}

abstract class $ModelResultDataCopyWith<$Res> {
  factory $ModelResultDataCopyWith(
          ModelResultData value, $Res Function(ModelResultData) then) =
      _$ModelResultDataCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'width') int width,
      @JsonKey(name: 'height') int height,
      @JsonKey(name: 'url') String url,
      @JsonKey(name: 'photographer') String photographer,
      @JsonKey(name: 'photographer_url') String photographerUrl,
      @JsonKey(name: 'photographer_id') int photographerId,
      @JsonKey(name: 'avg_color') String avgColor,
      @JsonKey(name: 'liked') bool liked,
      @JsonKey(name: 'src') SrcData src,
      @JsonKey(name: 'total_results') int totalResults});
}

class _$ModelResultDataCopyWithImpl<$Res> extends _$ModelDataCopyWithImpl<$Res>
    implements $ModelResultDataCopyWith<$Res> {
  _$ModelResultDataCopyWithImpl(
      ModelResultData _value, $Res Function(ModelResultData) _then)
      : super(_value, (v) => _then(v as ModelResultData));

  @override
  ModelResultData get _value => super._value as ModelResultData;

  @override
  $Res call({
    Object id = freezed,
    Object width = freezed,
    Object height = freezed,
    Object url = freezed,
    Object photographer = freezed,
    Object photographerUrl = freezed,
    Object photographerId = freezed,
    Object avgColor = freezed,
    Object liked = freezed,
    Object src = freezed,
    Object totalResults = freezed,
  }) {
    return _then(ModelResultData(
      id: id == freezed ? _value.id : id as int,
      width: width == freezed ? _value.width : width as int,
      height: height == freezed ? _value.height : height as int,
      url: url == freezed ? _value.url : url as String,
      photographer: photographer == freezed
          ? _value.photographer
          : photographer as String,
      photographerUrl: photographerUrl == freezed
          ? _value.photographerUrl
          : photographerUrl as String,
      photographerId: photographerId == freezed
          ? _value.photographerId
          : photographerId as int,
      avgColor: avgColor == freezed ? _value.avgColor : avgColor as String,
      liked: liked == freezed ? _value.liked : liked as bool,
      src: src == freezed ? _value.src : src as SrcData,
      totalResults:
          totalResults == freezed ? _value.totalResults : totalResults as int,
    ));
  }
}

@JsonSerializable()
class _$ModelResultData implements ModelResultData {
  _$ModelResultData(
      {@JsonKey(name: 'id') this.id,
      @JsonKey(name: 'width') this.width,
      @JsonKey(name: 'height') this.height,
      @JsonKey(name: 'url') this.url,
      @JsonKey(name: 'photographer') this.photographer,
      @JsonKey(name: 'photographer_url') this.photographerUrl,
      @JsonKey(name: 'photographer_id') this.photographerId,
      @JsonKey(name: 'avg_color') this.avgColor,
      @JsonKey(name: 'liked') this.liked,
      @JsonKey(name: 'src') this.src,
      @JsonKey(name: 'total_results') this.totalResults});

  factory _$ModelResultData.fromJson(Map<String, dynamic> json) =>
      _$_$ModelResultDataFromJson(json);

  @override
  @JsonKey(name: 'id')
  final int id;
  @override
  @JsonKey(name: 'width')
  final int width;
  @override
  @JsonKey(name: 'height')
  final int height;
  @override
  @JsonKey(name: 'url')
  final String url;
  @override
  @JsonKey(name: 'photographer')
  final String photographer;
  @override
  @JsonKey(name: 'photographer_url')
  final String photographerUrl;
  @override
  @JsonKey(name: 'photographer_id')
  final int photographerId;
  @override
  @JsonKey(name: 'avg_color')
  final String avgColor;
  @override
  @JsonKey(name: 'liked')
  final bool liked;
  @override
  @JsonKey(name: 'src')
  final SrcData src;
  @override
  @JsonKey(name: 'total_results')
  final int totalResults;

  @override
  String toString() {
    return 'ModelData.photosResultData(id: $id, width: $width, height: $height, url: $url, photographer: $photographer, photographerUrl: $photographerUrl, photographerId: $photographerId, avgColor: $avgColor, liked: $liked, src: $src, totalResults: $totalResults)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is ModelResultData &&
            (identical(other.id, id) ||
                const DeepCollectionEquality().equals(other.id, id)) &&
            (identical(other.width, width) ||
                const DeepCollectionEquality().equals(other.width, width)) &&
            (identical(other.height, height) ||
                const DeepCollectionEquality().equals(other.height, height)) &&
            (identical(other.url, url) ||
                const DeepCollectionEquality().equals(other.url, url)) &&
            (identical(other.photographer, photographer) ||
                const DeepCollectionEquality()
                    .equals(other.photographer, photographer)) &&
            (identical(other.photographerUrl, photographerUrl) ||
                const DeepCollectionEquality()
                    .equals(other.photographerUrl, photographerUrl)) &&
            (identical(other.photographerId, photographerId) ||
                const DeepCollectionEquality()
                    .equals(other.photographerId, photographerId)) &&
            (identical(other.avgColor, avgColor) ||
                const DeepCollectionEquality()
                    .equals(other.avgColor, avgColor)) &&
            (identical(other.liked, liked) ||
                const DeepCollectionEquality().equals(other.liked, liked)) &&
            (identical(other.src, src) ||
                const DeepCollectionEquality().equals(other.src, src)) &&
            (identical(other.totalResults, totalResults) ||
                const DeepCollectionEquality()
                    .equals(other.totalResults, totalResults)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(id) ^
      const DeepCollectionEquality().hash(width) ^
      const DeepCollectionEquality().hash(height) ^
      const DeepCollectionEquality().hash(url) ^
      const DeepCollectionEquality().hash(photographer) ^
      const DeepCollectionEquality().hash(photographerUrl) ^
      const DeepCollectionEquality().hash(photographerId) ^
      const DeepCollectionEquality().hash(avgColor) ^
      const DeepCollectionEquality().hash(liked) ^
      const DeepCollectionEquality().hash(src) ^
      const DeepCollectionEquality().hash(totalResults);

  @override
  $ModelResultDataCopyWith<ModelResultData> get copyWith =>
      _$ModelResultDataCopyWithImpl<ModelResultData>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required
        Result photosResultData(
            @JsonKey(name: 'id') int id,
            @JsonKey(name: 'width') int width,
            @JsonKey(name: 'height') int height,
            @JsonKey(name: 'url') String url,
            @JsonKey(name: 'photographer') String photographer,
            @JsonKey(name: 'photographer_url') String photographerUrl,
            @JsonKey(name: 'photographer_id') int photographerId,
            @JsonKey(name: 'avg_color') String avgColor,
            @JsonKey(name: 'liked') bool liked,
            @JsonKey(name: 'src') SrcData src,
            @JsonKey(name: 'total_results') int totalResults),
    @required
        Result srcData(
            @JsonKey(name: 'original') String original,
            @JsonKey(name: 'large2x') String large2x,
            @JsonKey(name: 'large') String large,
            @JsonKey(name: 'medium') String medium,
            @JsonKey(name: 'small') String small,
            @JsonKey(name: 'portrait') String portrait,
            @JsonKey(name: 'landscape') String landscape,
            @JsonKey(name: 'tiny') String tiny),
  }) {
    assert(photosResultData != null);
    assert(srcData != null);
    return photosResultData(id, width, height, url, photographer,
        photographerUrl, photographerId, avgColor, liked, src, totalResults);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result photosResultData(
        @JsonKey(name: 'id') int id,
        @JsonKey(name: 'width') int width,
        @JsonKey(name: 'height') int height,
        @JsonKey(name: 'url') String url,
        @JsonKey(name: 'photographer') String photographer,
        @JsonKey(name: 'photographer_url') String photographerUrl,
        @JsonKey(name: 'photographer_id') int photographerId,
        @JsonKey(name: 'avg_color') String avgColor,
        @JsonKey(name: 'liked') bool liked,
        @JsonKey(name: 'src') SrcData src,
        @JsonKey(name: 'total_results') int totalResults),
    Result srcData(
        @JsonKey(name: 'original') String original,
        @JsonKey(name: 'large2x') String large2x,
        @JsonKey(name: 'large') String large,
        @JsonKey(name: 'medium') String medium,
        @JsonKey(name: 'small') String small,
        @JsonKey(name: 'portrait') String portrait,
        @JsonKey(name: 'landscape') String landscape,
        @JsonKey(name: 'tiny') String tiny),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (photosResultData != null) {
      return photosResultData(id, width, height, url, photographer,
          photographerUrl, photographerId, avgColor, liked, src, totalResults);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result photosResultData(ModelResultData value),
    @required Result srcData(SrcData value),
  }) {
    assert(photosResultData != null);
    assert(srcData != null);
    return photosResultData(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result photosResultData(ModelResultData value),
    Result srcData(SrcData value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (photosResultData != null) {
      return photosResultData(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$_$ModelResultDataToJson(this)
      ..['runtimeType'] = 'photosResultData';
  }
}

abstract class ModelResultData implements ModelData {
  factory ModelResultData(
      {@JsonKey(name: 'id') int id,
      @JsonKey(name: 'width') int width,
      @JsonKey(name: 'height') int height,
      @JsonKey(name: 'url') String url,
      @JsonKey(name: 'photographer') String photographer,
      @JsonKey(name: 'photographer_url') String photographerUrl,
      @JsonKey(name: 'photographer_id') int photographerId,
      @JsonKey(name: 'avg_color') String avgColor,
      @JsonKey(name: 'liked') bool liked,
      @JsonKey(name: 'src') SrcData src,
      @JsonKey(name: 'total_results') int totalResults}) = _$ModelResultData;

  factory ModelResultData.fromJson(Map<String, dynamic> json) =
      _$ModelResultData.fromJson;

  @JsonKey(name: 'id')
  int get id;
  @JsonKey(name: 'width')
  int get width;
  @JsonKey(name: 'height')
  int get height;
  @JsonKey(name: 'url')
  String get url;
  @JsonKey(name: 'photographer')
  String get photographer;
  @JsonKey(name: 'photographer_url')
  String get photographerUrl;
  @JsonKey(name: 'photographer_id')
  int get photographerId;
  @JsonKey(name: 'avg_color')
  String get avgColor;
  @JsonKey(name: 'liked')
  bool get liked;
  @JsonKey(name: 'src')
  SrcData get src;
  @JsonKey(name: 'total_results')
  int get totalResults;
  $ModelResultDataCopyWith<ModelResultData> get copyWith;
}

abstract class $SrcDataCopyWith<$Res> {
  factory $SrcDataCopyWith(SrcData value, $Res Function(SrcData) then) =
      _$SrcDataCopyWithImpl<$Res>;
  $Res call(
      {@JsonKey(name: 'original') String original,
      @JsonKey(name: 'large2x') String large2x,
      @JsonKey(name: 'large') String large,
      @JsonKey(name: 'medium') String medium,
      @JsonKey(name: 'small') String small,
      @JsonKey(name: 'portrait') String portrait,
      @JsonKey(name: 'landscape') String landscape,
      @JsonKey(name: 'tiny') String tiny});
}

class _$SrcDataCopyWithImpl<$Res> extends _$ModelDataCopyWithImpl<$Res>
    implements $SrcDataCopyWith<$Res> {
  _$SrcDataCopyWithImpl(SrcData _value, $Res Function(SrcData) _then)
      : super(_value, (v) => _then(v as SrcData));

  @override
  SrcData get _value => super._value as SrcData;

  @override
  $Res call({
    Object original = freezed,
    Object large2x = freezed,
    Object large = freezed,
    Object medium = freezed,
    Object small = freezed,
    Object portrait = freezed,
    Object landscape = freezed,
    Object tiny = freezed,
  }) {
    return _then(SrcData(
      original: original == freezed ? _value.original : original as String,
      large2x: large2x == freezed ? _value.large2x : large2x as String,
      large: large == freezed ? _value.large : large as String,
      medium: medium == freezed ? _value.medium : medium as String,
      small: small == freezed ? _value.small : small as String,
      portrait: portrait == freezed ? _value.portrait : portrait as String,
      landscape: landscape == freezed ? _value.landscape : landscape as String,
      tiny: tiny == freezed ? _value.tiny : tiny as String,
    ));
  }
}

@JsonSerializable()
class _$SrcData implements SrcData {
  _$SrcData(
      {@JsonKey(name: 'original') this.original,
      @JsonKey(name: 'large2x') this.large2x,
      @JsonKey(name: 'large') this.large,
      @JsonKey(name: 'medium') this.medium,
      @JsonKey(name: 'small') this.small,
      @JsonKey(name: 'portrait') this.portrait,
      @JsonKey(name: 'landscape') this.landscape,
      @JsonKey(name: 'tiny') this.tiny});

  factory _$SrcData.fromJson(Map<String, dynamic> json) =>
      _$_$SrcDataFromJson(json);

  @override
  @JsonKey(name: 'original')
  final String original;
  @override
  @JsonKey(name: 'large2x')
  final String large2x;
  @override
  @JsonKey(name: 'large')
  final String large;
  @override
  @JsonKey(name: 'medium')
  final String medium;
  @override
  @JsonKey(name: 'small')
  final String small;
  @override
  @JsonKey(name: 'portrait')
  final String portrait;
  @override
  @JsonKey(name: 'landscape')
  final String landscape;
  @override
  @JsonKey(name: 'tiny')
  final String tiny;

  @override
  String toString() {
    return 'ModelData.srcData(original: $original, large2x: $large2x, large: $large, medium: $medium, small: $small, portrait: $portrait, landscape: $landscape, tiny: $tiny)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SrcData &&
            (identical(other.original, original) ||
                const DeepCollectionEquality()
                    .equals(other.original, original)) &&
            (identical(other.large2x, large2x) ||
                const DeepCollectionEquality()
                    .equals(other.large2x, large2x)) &&
            (identical(other.large, large) ||
                const DeepCollectionEquality().equals(other.large, large)) &&
            (identical(other.medium, medium) ||
                const DeepCollectionEquality().equals(other.medium, medium)) &&
            (identical(other.small, small) ||
                const DeepCollectionEquality().equals(other.small, small)) &&
            (identical(other.portrait, portrait) ||
                const DeepCollectionEquality()
                    .equals(other.portrait, portrait)) &&
            (identical(other.landscape, landscape) ||
                const DeepCollectionEquality()
                    .equals(other.landscape, landscape)) &&
            (identical(other.tiny, tiny) ||
                const DeepCollectionEquality().equals(other.tiny, tiny)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(original) ^
      const DeepCollectionEquality().hash(large2x) ^
      const DeepCollectionEquality().hash(large) ^
      const DeepCollectionEquality().hash(medium) ^
      const DeepCollectionEquality().hash(small) ^
      const DeepCollectionEquality().hash(portrait) ^
      const DeepCollectionEquality().hash(landscape) ^
      const DeepCollectionEquality().hash(tiny);

  @override
  $SrcDataCopyWith<SrcData> get copyWith =>
      _$SrcDataCopyWithImpl<SrcData>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required
        Result photosResultData(
            @JsonKey(name: 'id') int id,
            @JsonKey(name: 'width') int width,
            @JsonKey(name: 'height') int height,
            @JsonKey(name: 'url') String url,
            @JsonKey(name: 'photographer') String photographer,
            @JsonKey(name: 'photographer_url') String photographerUrl,
            @JsonKey(name: 'photographer_id') int photographerId,
            @JsonKey(name: 'avg_color') String avgColor,
            @JsonKey(name: 'liked') bool liked,
            @JsonKey(name: 'src') SrcData src,
            @JsonKey(name: 'total_results') int totalResults),
    @required
        Result srcData(
            @JsonKey(name: 'original') String original,
            @JsonKey(name: 'large2x') String large2x,
            @JsonKey(name: 'large') String large,
            @JsonKey(name: 'medium') String medium,
            @JsonKey(name: 'small') String small,
            @JsonKey(name: 'portrait') String portrait,
            @JsonKey(name: 'landscape') String landscape,
            @JsonKey(name: 'tiny') String tiny),
  }) {
    assert(photosResultData != null);
    assert(srcData != null);
    return srcData(
        original, large2x, large, medium, small, portrait, landscape, tiny);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result photosResultData(
        @JsonKey(name: 'id') int id,
        @JsonKey(name: 'width') int width,
        @JsonKey(name: 'height') int height,
        @JsonKey(name: 'url') String url,
        @JsonKey(name: 'photographer') String photographer,
        @JsonKey(name: 'photographer_url') String photographerUrl,
        @JsonKey(name: 'photographer_id') int photographerId,
        @JsonKey(name: 'avg_color') String avgColor,
        @JsonKey(name: 'liked') bool liked,
        @JsonKey(name: 'src') SrcData src,
        @JsonKey(name: 'total_results') int totalResults),
    Result srcData(
        @JsonKey(name: 'original') String original,
        @JsonKey(name: 'large2x') String large2x,
        @JsonKey(name: 'large') String large,
        @JsonKey(name: 'medium') String medium,
        @JsonKey(name: 'small') String small,
        @JsonKey(name: 'portrait') String portrait,
        @JsonKey(name: 'landscape') String landscape,
        @JsonKey(name: 'tiny') String tiny),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (srcData != null) {
      return srcData(
          original, large2x, large, medium, small, portrait, landscape, tiny);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result photosResultData(ModelResultData value),
    @required Result srcData(SrcData value),
  }) {
    assert(photosResultData != null);
    assert(srcData != null);
    return srcData(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result photosResultData(ModelResultData value),
    Result srcData(SrcData value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (srcData != null) {
      return srcData(this);
    }
    return orElse();
  }

  @override
  Map<String, dynamic> toJson() {
    return _$_$SrcDataToJson(this)..['runtimeType'] = 'srcData';
  }
}

abstract class SrcData implements ModelData {
  factory SrcData(
      {@JsonKey(name: 'original') String original,
      @JsonKey(name: 'large2x') String large2x,
      @JsonKey(name: 'large') String large,
      @JsonKey(name: 'medium') String medium,
      @JsonKey(name: 'small') String small,
      @JsonKey(name: 'portrait') String portrait,
      @JsonKey(name: 'landscape') String landscape,
      @JsonKey(name: 'tiny') String tiny}) = _$SrcData;

  factory SrcData.fromJson(Map<String, dynamic> json) = _$SrcData.fromJson;

  @JsonKey(name: 'original')
  String get original;
  @JsonKey(name: 'large2x')
  String get large2x;
  @JsonKey(name: 'large')
  String get large;
  @JsonKey(name: 'medium')
  String get medium;
  @JsonKey(name: 'small')
  String get small;
  @JsonKey(name: 'portrait')
  String get portrait;
  @JsonKey(name: 'landscape')
  String get landscape;
  @JsonKey(name: 'tiny')
  String get tiny;
  $SrcDataCopyWith<SrcData> get copyWith;
}
