import 'package:awesome_app/application/app/app_failure.dart';
import 'package:dartz/dartz.dart';
import 'model_reg.dart';

abstract class AppInterface {
  ///====================================================
  /// Photos
  ///====================================================
  Future<Either<AppFailure, ModelDataPhotos>> getPhotos({int perpage});
}
