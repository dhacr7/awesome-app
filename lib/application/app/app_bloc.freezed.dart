// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies

part of 'app_bloc.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$AppEventTearOff {
  const _$AppEventTearOff();

// ignore: unused_element
  GetPhotosData getPhotosData({int perpage}) {
    return GetPhotosData(
      perpage: perpage,
    );
  }
}

// ignore: unused_element
const $AppEvent = _$AppEventTearOff();

mixin _$AppEvent {
  int get perpage;

  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result getPhotosData(int perpage),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result getPhotosData(int perpage),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result getPhotosData(GetPhotosData value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result getPhotosData(GetPhotosData value),
    @required Result orElse(),
  });

  $AppEventCopyWith<AppEvent> get copyWith;
}

abstract class $AppEventCopyWith<$Res> {
  factory $AppEventCopyWith(AppEvent value, $Res Function(AppEvent) then) =
      _$AppEventCopyWithImpl<$Res>;
  $Res call({int perpage});
}

class _$AppEventCopyWithImpl<$Res> implements $AppEventCopyWith<$Res> {
  _$AppEventCopyWithImpl(this._value, this._then);

  final AppEvent _value;
  // ignore: unused_field
  final $Res Function(AppEvent) _then;

  @override
  $Res call({
    Object perpage = freezed,
  }) {
    return _then(_value.copyWith(
      perpage: perpage == freezed ? _value.perpage : perpage as int,
    ));
  }
}

abstract class $GetPhotosDataCopyWith<$Res> implements $AppEventCopyWith<$Res> {
  factory $GetPhotosDataCopyWith(
          GetPhotosData value, $Res Function(GetPhotosData) then) =
      _$GetPhotosDataCopyWithImpl<$Res>;
  @override
  $Res call({int perpage});
}

class _$GetPhotosDataCopyWithImpl<$Res> extends _$AppEventCopyWithImpl<$Res>
    implements $GetPhotosDataCopyWith<$Res> {
  _$GetPhotosDataCopyWithImpl(
      GetPhotosData _value, $Res Function(GetPhotosData) _then)
      : super(_value, (v) => _then(v as GetPhotosData));

  @override
  GetPhotosData get _value => super._value as GetPhotosData;

  @override
  $Res call({
    Object perpage = freezed,
  }) {
    return _then(GetPhotosData(
      perpage: perpage == freezed ? _value.perpage : perpage as int,
    ));
  }
}

class _$GetPhotosData implements GetPhotosData {
  _$GetPhotosData({this.perpage});

  @override
  final int perpage;

  @override
  String toString() {
    return 'AppEvent.getPhotosData(perpage: $perpage)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is GetPhotosData &&
            (identical(other.perpage, perpage) ||
                const DeepCollectionEquality().equals(other.perpage, perpage)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(perpage);

  @override
  $GetPhotosDataCopyWith<GetPhotosData> get copyWith =>
      _$GetPhotosDataCopyWithImpl<GetPhotosData>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result getPhotosData(int perpage),
  }) {
    assert(getPhotosData != null);
    return getPhotosData(perpage);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result getPhotosData(int perpage),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (getPhotosData != null) {
      return getPhotosData(perpage);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result getPhotosData(GetPhotosData value),
  }) {
    assert(getPhotosData != null);
    return getPhotosData(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result getPhotosData(GetPhotosData value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (getPhotosData != null) {
      return getPhotosData(this);
    }
    return orElse();
  }
}

abstract class GetPhotosData implements AppEvent {
  factory GetPhotosData({int perpage}) = _$GetPhotosData;

  @override
  int get perpage;
  @override
  $GetPhotosDataCopyWith<GetPhotosData> get copyWith;
}

class _$AppStateTearOff {
  const _$AppStateTearOff();

// ignore: unused_element
  _Initial initial() {
    return _Initial();
  }

// ignore: unused_element
  _ModelDataOptional photosDataOptional(
      {@required bool onLoading,
      @required Option<Either<AppFailure, ModelDataPhotos>> dataModel}) {
    return _ModelDataOptional(
      onLoading: onLoading,
      dataModel: dataModel,
    );
  }
}

// ignore: unused_element
const $AppState = _$AppStateTearOff();

mixin _$AppState {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initial(),
    @required
        Result photosDataOptional(bool onLoading,
            Option<Either<AppFailure, ModelDataPhotos>> dataModel),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initial(),
    Result photosDataOptional(
        bool onLoading, Option<Either<AppFailure, ModelDataPhotos>> dataModel),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initial(_Initial value),
    @required Result photosDataOptional(_ModelDataOptional value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initial(_Initial value),
    Result photosDataOptional(_ModelDataOptional value),
    @required Result orElse(),
  });
}

abstract class $AppStateCopyWith<$Res> {
  factory $AppStateCopyWith(AppState value, $Res Function(AppState) then) =
      _$AppStateCopyWithImpl<$Res>;
}

class _$AppStateCopyWithImpl<$Res> implements $AppStateCopyWith<$Res> {
  _$AppStateCopyWithImpl(this._value, this._then);

  final AppState _value;
  // ignore: unused_field
  final $Res Function(AppState) _then;
}

abstract class _$InitialCopyWith<$Res> {
  factory _$InitialCopyWith(_Initial value, $Res Function(_Initial) then) =
      __$InitialCopyWithImpl<$Res>;
}

class __$InitialCopyWithImpl<$Res> extends _$AppStateCopyWithImpl<$Res>
    implements _$InitialCopyWith<$Res> {
  __$InitialCopyWithImpl(_Initial _value, $Res Function(_Initial) _then)
      : super(_value, (v) => _then(v as _Initial));

  @override
  _Initial get _value => super._value as _Initial;
}

class _$_Initial implements _Initial {
  _$_Initial();

  @override
  String toString() {
    return 'AppState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is _Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initial(),
    @required
        Result photosDataOptional(bool onLoading,
            Option<Either<AppFailure, ModelDataPhotos>> dataModel),
  }) {
    assert(initial != null);
    assert(photosDataOptional != null);
    return initial();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initial(),
    Result photosDataOptional(
        bool onLoading, Option<Either<AppFailure, ModelDataPhotos>> dataModel),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initial(_Initial value),
    @required Result photosDataOptional(_ModelDataOptional value),
  }) {
    assert(initial != null);
    assert(photosDataOptional != null);
    return initial(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initial(_Initial value),
    Result photosDataOptional(_ModelDataOptional value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class _Initial implements AppState {
  factory _Initial() = _$_Initial;
}

abstract class _$ModelDataOptionalCopyWith<$Res> {
  factory _$ModelDataOptionalCopyWith(
          _ModelDataOptional value, $Res Function(_ModelDataOptional) then) =
      __$ModelDataOptionalCopyWithImpl<$Res>;
  $Res call(
      {bool onLoading, Option<Either<AppFailure, ModelDataPhotos>> dataModel});
}

class __$ModelDataOptionalCopyWithImpl<$Res>
    extends _$AppStateCopyWithImpl<$Res>
    implements _$ModelDataOptionalCopyWith<$Res> {
  __$ModelDataOptionalCopyWithImpl(
      _ModelDataOptional _value, $Res Function(_ModelDataOptional) _then)
      : super(_value, (v) => _then(v as _ModelDataOptional));

  @override
  _ModelDataOptional get _value => super._value as _ModelDataOptional;

  @override
  $Res call({
    Object onLoading = freezed,
    Object dataModel = freezed,
  }) {
    return _then(_ModelDataOptional(
      onLoading: onLoading == freezed ? _value.onLoading : onLoading as bool,
      dataModel: dataModel == freezed
          ? _value.dataModel
          : dataModel as Option<Either<AppFailure, ModelDataPhotos>>,
    ));
  }
}

class _$_ModelDataOptional implements _ModelDataOptional {
  _$_ModelDataOptional({@required this.onLoading, @required this.dataModel})
      : assert(onLoading != null),
        assert(dataModel != null);

  @override
  final bool onLoading;
  @override
  final Option<Either<AppFailure, ModelDataPhotos>> dataModel;

  @override
  String toString() {
    return 'AppState.photosDataOptional(onLoading: $onLoading, dataModel: $dataModel)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is _ModelDataOptional &&
            (identical(other.onLoading, onLoading) ||
                const DeepCollectionEquality()
                    .equals(other.onLoading, onLoading)) &&
            (identical(other.dataModel, dataModel) ||
                const DeepCollectionEquality()
                    .equals(other.dataModel, dataModel)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^
      const DeepCollectionEquality().hash(onLoading) ^
      const DeepCollectionEquality().hash(dataModel);

  @override
  _$ModelDataOptionalCopyWith<_ModelDataOptional> get copyWith =>
      __$ModelDataOptionalCopyWithImpl<_ModelDataOptional>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initial(),
    @required
        Result photosDataOptional(bool onLoading,
            Option<Either<AppFailure, ModelDataPhotos>> dataModel),
  }) {
    assert(initial != null);
    assert(photosDataOptional != null);
    return photosDataOptional(onLoading, dataModel);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initial(),
    Result photosDataOptional(
        bool onLoading, Option<Either<AppFailure, ModelDataPhotos>> dataModel),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (photosDataOptional != null) {
      return photosDataOptional(onLoading, dataModel);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initial(_Initial value),
    @required Result photosDataOptional(_ModelDataOptional value),
  }) {
    assert(initial != null);
    assert(photosDataOptional != null);
    return photosDataOptional(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initial(_Initial value),
    Result photosDataOptional(_ModelDataOptional value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (photosDataOptional != null) {
      return photosDataOptional(this);
    }
    return orElse();
  }
}

abstract class _ModelDataOptional implements AppState {
  factory _ModelDataOptional(
          {@required bool onLoading,
          @required Option<Either<AppFailure, ModelDataPhotos>> dataModel}) =
      _$_ModelDataOptional;

  bool get onLoading;
  Option<Either<AppFailure, ModelDataPhotos>> get dataModel;
  _$ModelDataOptionalCopyWith<_ModelDataOptional> get copyWith;
}
