part of 'app_bloc.dart';

@freezed
abstract class AppEvent with _$AppEvent {
  ///====================================================
  /// Photos
  ///====================================================
  factory AppEvent.getPhotosData({int perpage}) = GetPhotosData;
}
