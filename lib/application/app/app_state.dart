part of 'app_bloc.dart';

@freezed
abstract class AppState with _$AppState {
  factory AppState.initial() = _Initial;

  ///====================================================
  /// Photos
  ///====================================================
  factory AppState.photosDataOptional(
          {@required bool onLoading,
          @required Option<Either<AppFailure, ModelDataPhotos>> dataModel}) =
      _ModelDataOptional;
}
