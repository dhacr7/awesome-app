import 'dart:async';
import 'package:awesome_app/application/app/app_failure.dart';
import 'package:awesome_app/domain/app/app_interface.dart';
import 'package:awesome_app/domain/app/model_reg.dart';
import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:injectable/injectable.dart';
import 'package:meta/meta.dart';

part 'app_bloc.freezed.dart';
part 'app_event.dart';
part 'app_state.dart';

@injectable
class AppBloc extends Bloc<AppEvent, AppState> {
  AppBloc(this._appInterface) : super(AppState.initial());
  final AppInterface _appInterface;

  @override
  Stream<AppState> mapEventToState(AppEvent event) async* {
    ///====================================================
    /// Photos
    ///====================================================
    yield* event.map(
      getPhotosData: (e) async* {
        yield AppState.photosDataOptional(
          onLoading: true,
          dataModel: none(),
        );
        final _result = await _appInterface.getPhotos(perpage: e.perpage);
        yield AppState.photosDataOptional(
            onLoading: false, dataModel: some(_result));
      },

      ///====================================================
    );
  }
}
