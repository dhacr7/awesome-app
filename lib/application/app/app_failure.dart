import 'package:freezed_annotation/freezed_annotation.dart';
part 'app_failure.freezed.dart';

@freezed
abstract class AppFailure with _$AppFailure {
  factory AppFailure.badRequest(String badRequest) = _BadRequest;
  const factory AppFailure.notFound(String msg) = _NotFound;
  const factory AppFailure.serverError() = _ServerError;
}
