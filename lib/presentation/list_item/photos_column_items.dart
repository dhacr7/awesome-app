import 'package:awesome_app/domain/app/model_data.dart';
import 'package:awesome_app/presentation/detail_page.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PhotoColumnItems extends StatelessWidget {
  final List<ModelResultData> modelDataPhotos;
  final int index;

  const PhotoColumnItems({
    Key key,
    @required this.modelDataPhotos,
    @required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: false,
      minimum: const EdgeInsets.only(
        left: 8,
        top: 8,
        bottom: 8,
        right: 8,
      ),
      child: Container(
        child: GestureDetector(
          onTap: () => (modelDataPhotos[index].src.landscape != null)
              ? Get.to(() => DetailPage(
                    selectionImageUrl: modelDataPhotos[index].src.landscape,
                    photographer: modelDataPhotos[index].photographer,
                  ))
              : Get.snackbar('Error', 'Item null not relevan',
                  snackPosition: SnackPosition.BOTTOM,
                  duration: Duration(seconds: 2)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.grey,
                    borderRadius: BorderRadius.circular(15),
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(4),
                    child: CachedNetworkImage(
                      memCacheHeight: 256,
                      memCacheWidth: 256,
                      filterQuality: FilterQuality.medium,
                      maxHeightDiskCache: 256,
                      maxWidthDiskCache: 256,
                      imageUrl: '${modelDataPhotos[index].src.portrait}',
                      fit: BoxFit.cover,
                      placeholder: (context, url) =>
                          Center(child: CupertinoActivityIndicator()),
                      errorWidget: (context, url, error) =>
                          Center(child: CupertinoActivityIndicator()),
                      width: 96,
                      height: 96,
                    ),
                  ),
                ),
              ),
              const SizedBox(height: 10),
              Text(
                '${modelDataPhotos[index].photographer}',
                style: TextStyle(
                  fontWeight: FontWeight.w600,
                  color: Colors.black87,
                  fontSize: 14,
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              ),
              Text(
                "Colection",
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.black54,
                ),
                maxLines: 1,
                overflow: TextOverflow.ellipsis,
              )
            ],
          ),
        ),
      ),
    );
  }
}
