import 'package:awesome_app/domain/app/model_data.dart';
import 'package:awesome_app/presentation/detail_page.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class PhotosRowItems extends StatelessWidget {
  final List<ModelResultData> modelDataPhotos;
  final int index;

  const PhotosRowItems({
    Key key,
    @required this.modelDataPhotos,
    @required this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: false,
      bottom: false,
      minimum: const EdgeInsets.only(
        left: 8,
        top: 8,
        bottom: 8,
        right: 8,
      ),
      child: Container(
        child: GestureDetector(
          onTap: () => (modelDataPhotos[index].src.landscape != null)
              ? Get.to(() => DetailPage(
                    selectionImageUrl: modelDataPhotos[index].src.landscape,
                    photographer: modelDataPhotos[index].photographer,
                  ))
              : Get.snackbar('Error', 'Item null not relevan',
                  snackPosition: SnackPosition.BOTTOM,
                  duration: Duration(seconds: 2)),
          child: Row(
            children: <Widget>[
              ClipRRect(
                borderRadius: BorderRadius.circular(4),
                child: CachedNetworkImage(
                  memCacheHeight: 256,
                  memCacheWidth: 256,
                  filterQuality: FilterQuality.medium,
                  maxHeightDiskCache: 256,
                  maxWidthDiskCache: 256,
                  imageUrl: '${modelDataPhotos[index].src.portrait}',
                  fit: BoxFit.cover,
                  placeholder: (context, url) =>
                      Center(child: CupertinoActivityIndicator()),
                  errorWidget: (context, url, error) =>
                      Center(child: CupertinoActivityIndicator()),
                  width: 96,
                  height: 96,
                ),
              ),
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 12),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        '${modelDataPhotos[index].photographer}',
                        style: TextStyle(
                          color: Colors.black87,
                          fontSize: 14,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      const Padding(padding: EdgeInsets.only(top: 8)),
                      Text(
                        '\Collection',
                        style: TextStyle(
                          fontSize: 12,
                          color: Colors.black54,
                        ),
                      )
                    ],
                  ),
                ),
              ),
              CupertinoButton(
                padding: EdgeInsets.zero,
                onPressed: () => (modelDataPhotos[index].src.landscape != null)
                    ? Get.to(() => DetailPage(
                          selectionImageUrl:
                              modelDataPhotos[index].src.landscape,
                          photographer: modelDataPhotos[index].photographer,
                        ))
                    : Get.snackbar('Error', 'Item null not relevan',
                        snackPosition: SnackPosition.BOTTOM,
                        duration: Duration(seconds: 2)),
                child: const Icon(
                  CupertinoIcons.chevron_right_square_fill,
                  semanticLabel: 'Add',
                  color: Colors.black12,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
