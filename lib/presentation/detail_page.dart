import 'package:awesome_app/presentation/widgets/banner_filter.dart';
import 'package:background_app_bar/background_app_bar.dart';
import 'package:flutter/material.dart';

class DetailPage extends StatelessWidget {
  final String selectionImageUrl;
  final String photographer;

  const DetailPage({
    Key key,
    @required this.selectionImageUrl,
    @required this.photographer,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(extendBodyBehindAppBar: false, body: _pageBodyMethod());
  }

  /// Page Body Method
  CustomScrollView _pageBodyMethod() {
    return CustomScrollView(
      slivers: <Widget>[
        selectionImageUrl != null
            ? SliverAppBar(
                elevation: 0,
                pinned: true,
                floating: true,
                snap: false,
                expandedHeight: 300,
                flexibleSpace: BackgroundFlexibleSpaceBar(
                  background: Container(
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: NetworkImage(
                          selectionImageUrl,
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                    child: BannerFilter(),
                  ),
                  title: Text('$photographer'),
                ),
              )
            : SliverAppBar(
                elevation: 0,
                pinned: true,
                floating: true,
                snap: false,
                title: Text('$photographer'),
              ),
        SliverList(
          delegate: SliverChildListDelegate(
            <Widget>[
              Padding(
                padding: const EdgeInsets.all(10.0),
                child: Container(
                  child: Text('Photographer Colection : ' + '$photographer',
                      style: TextStyle(color: Colors.black87, fontSize: 14.0)),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
