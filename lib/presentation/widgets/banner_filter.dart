import 'package:flutter/material.dart';

class BannerFilter extends StatelessWidget {
  final double borderRadius;
  final Widget child;

  const BannerFilter({
    Key key,
    this.borderRadius,
    this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(borderRadius ?? 0),
      child: Stack(
        children: [
          Container(
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Color(0xFF343434).withOpacity(0.4),
                  Color(0xFF343434).withOpacity(0.15),
                ],
              ),
            ),
          ),
          child ?? SizedBox(),
        ],
      ),
    );
  }
}
