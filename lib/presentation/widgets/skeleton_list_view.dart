import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class SkeletonListView extends StatelessWidget {
  final double height;
  final double width;
  final double allMargin;

  const SkeletonListView({
    Key key,
    this.height,
    this.width,
    this.allMargin,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) => Shimmer.fromColors(
        baseColor: Colors.grey[400],
        highlightColor: Colors.grey[100],
        child: Container(
          child: ListView.builder(
            itemCount: 20,
            itemBuilder: (BuildContext context, int index) {
              return Container(
                margin: EdgeInsets.all(allMargin ?? 0),
                height: height ?? 0,
                width: width ?? 0,
                decoration: BoxDecoration(
                  color: Colors.grey.withOpacity(0.2),
                  borderRadius: BorderRadius.circular(20),
                ),
              );
            },
          ),
        ),
      );
}
