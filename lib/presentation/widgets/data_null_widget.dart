import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DataNullWidget extends StatelessWidget {
  const DataNullWidget({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Container(
          height: 200.0,
          width: 2000.0,
          child: Column(
            children: [
              Icon(
                CupertinoIcons.zzz,
                size: 80.0,
                color: Colors.black26,
              ),
              SizedBox(
                height: 15.0,
              ),
              Text(
                "Opps No Data Response !!!",
                textAlign: TextAlign.center,
                style: TextStyle(
                  color: Colors.black45,
                  fontWeight: FontWeight.w500,
                  fontSize: 14.0,
                ),
                maxLines: 2,
                overflow: TextOverflow.ellipsis,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
