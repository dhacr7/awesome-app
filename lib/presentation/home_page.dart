import 'dart:math';
import 'package:awesome_app/application/app/app_bloc.dart';
import 'package:awesome_app/domain/app/model_data.dart';
import 'package:awesome_app/injection.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_staggered_animations/flutter_staggered_animations.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'list_item/photos_column_items.dart';
import 'list_item/photos_row_items.dart';
import 'widgets/banner_filter.dart';
import 'widgets/data_null_widget.dart';
import 'widgets/skeleton_list_view.dart';
import 'package:background_app_bar/background_app_bar.dart';
import 'package:awesome_app/utils/constants.dart' as constants;

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  /// Value notifier for list change
  final ValueNotifier<bool> _layoutListStatus = ValueNotifier<bool>(false);
  final List<ModelResultData> _mListRepository = [];
  final _scrollController = ScrollController();
  final _appBloc = getIt<AppBloc>();
  int _totalResult;
  int _pageRandom = 1;
  bool _loadMore;

  @override
  void initState() {
    // TODO: implement initState

    /// Get Data Total Result From Data Preference
    _getDataPref();
    _layoutListStatus.value = false;
    this._loadMore = false;
    super.initState();
  }

  /// Get Data Total Result From Data Preference
  void _getDataPref() async {
    /// Read Data Storage
    _totalResult = GetStorage().read(constants.TOTAL_RESULT_KEY);
    _executeTotalResult(_totalResult);

    /// Listen Key Value
    GetStorage().listenKey(constants.TOTAL_RESULT_KEY, (value) {
      _totalResult = value;
      _executeTotalResult(_totalResult);
    });
  }

  /// Execute Total Result
  void _executeTotalResult(int value) {
    if (value == null) {
      _pageRandom = 1;
    } else {
      _pageRandom = Random.secure().nextInt((value ~/ constants.PERPAGE));
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _layoutListStatus.dispose();
    _scrollController.dispose();
    this._loadMore = false;
    super.dispose();
  }

  /// BloC Listener
  void dataBLoCListener(BuildContext context, AppState state) {
    state.maybeMap(
      orElse: () {},
      photosDataOptional: (e) => e.dataModel.fold(
        () => () => DataNullWidget(),
        (a) => a.fold(
          (l) {
            l.maybeMap(
              badRequest: (e) => DataNullWidget(),
              orElse: () {},
            );
            Get.snackbar('Error', 'Data null not relevan',
                snackPosition: SnackPosition.BOTTOM,
                duration: Duration(seconds: 2));
          },
          (r) => {
            _mListRepository.addAll(r.photos),
            this._loadMore = false,
          },
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      extendBodyBehindAppBar: false,
      body: BlocProvider(
        create: (context) => _appBloc
          ..add(AppEvent.getPhotosData(
            perpage: _pageRandom,
          )),
        child: BlocConsumer<AppBloc, AppState>(
            builder: (context, state) {
              return state.maybeMap(
                /// Data None
                orElse: () => SkeletonListView(
                  height: 96,
                  width: double.infinity,
                  allMargin: 8.0,
                ),
                photosDataOptional: (e) {
                  if (e.onLoading) {
                    return _loadMore == true
                        ? _pageBodyMethod()
                        : SkeletonListView(
                            height: 96,
                            width: double.infinity,
                            allMargin: 8.0,
                          );
                  } else {
                    return e.dataModel.fold(
                      /// Data Null
                      () => DataNullWidget(),
                      (a) => a.fold(
                        (l) => DataNullWidget(),
                        (r) => _pageBodyMethod(),
                      ),
                    );
                  }
                },
              );
            },
            listener: dataBLoCListener),
      ),
    );
  }

  /// Page Body Method
  NotificationListener _pageBodyMethod() {
    return NotificationListener<ScrollNotification>(
      onNotification: _handleScrollNotification,
      child: CustomScrollView(
        controller: _scrollController,
        slivers: <Widget>[
          _mListRepository[0].src.landscape != null
              ? SliverAppBar(
                  elevation: 0,
                  pinned: true,
                  floating: true,
                  snap: false,
                  actions: [
                    /// Select Menu Grid
                    IconButton(
                      icon: Icon(
                        CupertinoIcons.rectangle_grid_2x2,
                        color: Colors.white,
                      ),
                      onPressed: () => _layoutListStatus.value = true,
                    ),

                    /// Select Menu List
                    IconButton(
                      icon: Icon(
                        CupertinoIcons.list_dash,
                        color: Colors.white,
                      ),
                      onPressed: () => _layoutListStatus.value = false,
                    ),
                  ],
                  expandedHeight: 200,
                  flexibleSpace: BackgroundFlexibleSpaceBar(
                    background: Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: NetworkImage(
                            _mListRepository[0].src.landscape,
                          ),
                          fit: BoxFit.cover,
                        ),
                      ),
                      child: BannerFilter(),
                    ),
                    title: Text('Awesome App'),
                  ),
                )
              : SliverAppBar(
                  elevation: 0,
                  pinned: true,
                  floating: true,
                  snap: false,
                  title: Text('Awesome App'),
                ),
          SliverList(
            delegate: SliverChildListDelegate(
              <Widget>[
                /// List Of Items
                _listLayoutMethod()
              ],
            ),
          ),
        ],
      ),
    );
  }

  /// List Layout Method
  SafeArea _listLayoutMethod() {
    return SafeArea(
      child: AnimationLimiter(
        /// List Of Items
        child: ValueListenableBuilder(
          valueListenable: _layoutListStatus,
          builder: (_, value, __) =>
              value == false ? _listItemMethod() : _gridtemMethod(),
        ),
      ),
    );
  }

  /// List Items Method
  Column _listItemMethod() {
    return Column(
      children: [
        ListView.builder(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          padding: const EdgeInsets.all(8.0),
          itemCount: _mListRepository.length,
          itemBuilder: (BuildContext context, int index) {
            return AnimationConfiguration.staggeredList(
              position: index,
              duration: const Duration(milliseconds: 375),
              child: SlideAnimation(
                verticalOffset: 44.0,
                child: FadeInAnimation(
                  child: PhotosRowItems(
                    modelDataPhotos: _mListRepository,
                    index: index,
                  ),
                ),
              ),
            );
          },
        ),
        _buildLoaderListItem(),
      ],
    );
  }

  /// Grid Items Method
  Column _gridtemMethod() {
    return Column(
      children: [
        GridView.count(
          shrinkWrap: true,
          physics: NeverScrollableScrollPhysics(),
          childAspectRatio: 1.0,
          padding: const EdgeInsets.all(8.0),
          crossAxisCount: 2,
          children: List.generate(
            _mListRepository.length,
            (int index) {
              return AnimationConfiguration.staggeredGrid(
                columnCount: _mListRepository.length,
                position: index,
                duration: const Duration(milliseconds: 375),
                child: ScaleAnimation(
                  scale: 0.5,
                  child: FadeInAnimation(
                    child: PhotoColumnItems(
                      modelDataPhotos: _mListRepository,
                      index: index,
                    ),
                  ),
                ),
              );
            },
          ),
        ),
        _buildLoaderListItem(),
      ],
    );
  }

  /// Handle Scroll Notification For Pagination
  bool _handleScrollNotification(ScrollNotification notification) {
    if (notification is ScrollEndNotification &&
        _scrollController.position.extentAfter == 0) {
      if (_totalResult != null && !_loadMore && _pageRandom < _totalResult) {
        this._loadMore = true;

        /// Parsing data pagination
        _appBloc..add(AppEvent.getPhotosData(perpage: _pageRandom++));
      }
    }
    return false;
  }

  /// Build Loader List Item
  Container _buildLoaderListItem() {
    return Container(
      child: Center(
        child: CupertinoActivityIndicator(),
      ),
    );
  }

  ///====================
}
