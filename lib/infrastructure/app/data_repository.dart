import 'package:awesome_app/application/app/app_failure.dart';
import 'package:awesome_app/domain/app/app_interface.dart';
import 'package:awesome_app/domain/app/model_reg.dart';
import 'package:awesome_app/utils/constants.dart' as constants;
import 'package:dartz/dartz.dart';
import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';
import 'package:get_storage/get_storage.dart';
import '../../injection.dart';

@Injectable(as: AppInterface)
class DataRepository extends AppInterface {
  Dio dio = getIt<Dio>();
  FormData formdata;
  Response response;

  DataRepository(this.dio, this.formdata, this.response);

  ///====================================================
  /// Photos
  ///====================================================
  @override
  Future<Either<AppFailure, ModelDataPhotos>> getPhotos({int perpage}) async {
    try {
      dio.options.headers["authorization"] = constants.AUTHORIZATION;
      response = await dio.get(constants.BASE_URL +
          'curated/?page=$perpage&per_page=' +
          '${constants.PERPAGE}');
      final _result = response.data;

      /// Set Data To Preference....
      GetStorage().write(constants.TOTAL_RESULT_KEY, _result['total_results']);

      ///===============================================
      final data = ModelDataPhotos.fromJson(_result);
      return right(data);
    } on DioError catch (err) {
      return left(AppFailure.badRequest(err.toString()));
    } catch (err) {
      return left(AppFailure.badRequest(err.toString()));
    }
  }

  ///===========================================
}
