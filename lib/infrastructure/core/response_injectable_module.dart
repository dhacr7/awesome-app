import 'package:dio/dio.dart';
import 'package:injectable/injectable.dart';

@module
abstract class ResponseDataInjectableModule {
  @lazySingleton
  Response get response => Response();
}
