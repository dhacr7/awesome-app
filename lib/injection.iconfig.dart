// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:awesome_app/infrastructure/core/dio_injectable_module.dart';
import 'package:dio/dio.dart';
import 'package:awesome_app/infrastructure/core/form_data_injectable_module.dart';
import 'package:awesome_app/infrastructure/core/response_injectable_module.dart';
import 'package:awesome_app/infrastructure/app/data_repository.dart';
import 'package:awesome_app/domain/app/app_interface.dart';
import 'package:awesome_app/application/app/app_bloc.dart';
import 'package:get_it/get_it.dart';

void $initGetIt(GetIt g, {String environment}) {
  final dioInjectableModule = _$DioInjectableModule();
  final formDataInjectableModule = _$FormDataInjectableModule();
  final responseDataInjectableModule = _$ResponseDataInjectableModule();
  g.registerLazySingleton<Dio>(() => dioInjectableModule.dio);
  g.registerLazySingleton<FormData>(() => formDataInjectableModule.formdata);
  g.registerLazySingleton<Response<dynamic>>(
      () => responseDataInjectableModule.response);
  g.registerFactory<AppInterface>(() => DataRepository(
        g<Dio>(),
        g<FormData>(),
        g<Response<dynamic>>(),
      ));
  g.registerFactory<AppBloc>(() => AppBloc(g<AppInterface>()));
}

class _$DioInjectableModule extends DioInjectableModule {}

class _$FormDataInjectableModule extends FormDataInjectableModule {}

class _$ResponseDataInjectableModule extends ResponseDataInjectableModule {}
